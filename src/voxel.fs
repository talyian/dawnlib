namespace DawnLib
open DawnLib.Mesh
open DawnLib.Core

module Voxels =
    (** One single block *)
    type Block = struct
        [<DefaultValue>]
        val mutable value: byte
    end

    type BlockExtent = struct
        val mutable block: Block
        val mutable dims: Vector3i
        override this.ToString () = sprintf "(%d,%d,%d,%O)" this.dims.X this.dims.Y this.dims.Z this.block
    end

    type BlockChunk = struct
        val mutable data: Block[]
        val mutable dims: Vector3i
        val mutable offset: Vector3i
        val mutable palette: OpenTK.Graphics.Color4 []
        static member FromSeq (pos) (w,h,d) s =
            let index a b c = a + w * (b + h * c)
            let dims = Vector3i(w, h, d)
            let data = Array.zeroCreate (w * h * d)
            for a,b,c,d in s do data.[index a b c] <- d
            new BlockChunk(data=data,dims=dims,offset=pos)

        member this.ToExtents () =
            let w,h,d = this.dims.X, this.dims.Y, this.dims.Z
            let index a b c = a + w * (b + h * c)
            let mutable extents = Array.map (fun x -> BlockExtent(block=x,dims=Vector3i(1,1,1))) this.data
            for i in 0 .. w-1 do
                for j in 0 .. h-1 do
                    let rec check_z start stop =
                        if stop >= d then ()
                        elif extents.[index i j start].block.value = 0uy then check_z stop (stop + 1)
                        elif extents.[index i j start].block.value = extents.[index i j stop].block.value then
                            extents.[index i j start].dims <- extents.[index i j start].dims + Vector3i(0, 0, 1)
                            extents.[index i j stop].dims <- Vector3i()
                            check_z start (stop + 1)
                        else check_z stop (stop + 1)
                    check_z 0 1
            for i in 0 .. w-1 do
                for k in 0 .. d-1 do
                    let rec check_y start stop =
                        if stop >= h then ()
                        else
                            let st = extents.[index i start k]
                            let ss = extents.[index i stop k]
                            if ss.dims.Y = 0 || st.block.value = 0uy || st.block.value <> ss.block.value then
                                check_y stop (stop + 1)
                            elif st.dims.X = ss.dims.X && ss.dims.Z = st.dims.Z then
                                extents.[index i start k].dims <- extents.[index i start k].dims + Vector3i(0, 1, 0)
                                extents.[index i stop k].dims <- Vector3i()
                                check_y start (stop + 1)
                            else
                                check_y stop (stop + 1)
                    check_y 0 1
            for j in 0 .. h-1 do
                for k in 0 .. d-1 do
                    let rec check_x start stop =
                        if stop >= w then ()
                        else
                            let st = extents.[index start j k]
                            let ss = extents.[index stop j k]
                            if ss.dims.Y = 0 || st.block.value = 0uy || st.block.value <> ss.block.value then
                                check_x stop (stop + 1)
                            elif st.dims.Y = ss.dims.Y && ss.dims.Z = st.dims.Z then
                                extents.[index start j k].dims <- extents.[index start j k].dims + Vector3i(1, 0, 0)
                                extents.[index stop j k].dims <- Vector3i()
                                check_x start (stop + 1)
                            else
                                check_x stop (stop + 1)
                    check_x 0 1
            [| for i in 0 .. w-1 do
               for j in 0..h-1 do
               for k in 0..d-1 do
                  let be = extents.[index i j k]
                  if be.block.value > 0uy && be.dims.X > 0 then yield (Vector3i(i,j,k), be.dims, be.block) |]
    end

    let toChunks size inputStream =
        inputStream
        |> Seq.groupBy (fun (a,b,c,d) -> Vector3i(a / size ,b / size,c / size))
        |> Seq.map (fun (min, items) ->
                    let min = min * size
                    BlockChunk.FromSeq min (size,size,size) (seq { for (a,b,c,d) in items -> a-min.X, b-min.Y, c-min.Z, d }))

    let chunkToMesh (chunk:BlockChunk) =
        let extents = chunk.ToExtents()
        let m = Mesh()
        for (p,d,x) in extents do
            let s = 0.1f
            let p = p + chunk.offset
            let pos = OpenTK.Vector3(float32 p.X, float32 p.Y, float32 p.Z)
            let a, b, c = (s * OpenTK.Vector3.UnitX * float32 d.X,
                           s * OpenTK.Vector3.UnitY * float32 d.Y,
                           s * OpenTK.Vector3.UnitZ * float32 d.Z)
            let pos = s * pos;
            let col = Array.get chunk.palette (int x.value - 1)
            let verts = [ pos; pos + c; pos + b; pos + b + c;
                          pos + a; pos + c + a; pos + b + a; pos + b + c + a]
            let addvert i = m.vertices.Add(new Vertex(col=col, pos=verts.[i]))
            let addidx i = m.indices.Add i
            let addface (a,b,c,d) =
                let vv = m.vertices.Count
                addvert a
                addvert b
                addvert c
                addvert d
                addidx vv
                addidx (vv+2)
                addidx (vv+1)
                addidx (vv+1)
                addidx (vv+2)
                addidx (vv+3)
            addface (0,1,2,3)
            addface (4,5,6,7)
            addface (0,4,1,5)
            addface (2,6,3,7)
            addface (0,2,4,6)
            addface (1,3,5,7)
        m.CalcNormals()
        m
