module DawnLib.Apps.RenderApp

open OpenTK
open OpenTK.Graphics
open OpenTK.Graphics.OpenGL

open DawnLib.Core
type Mesh =  DawnLib.Mesh.Mesh
type VBO = DawnLib.Renderers.GL3.VBO

(** A Standalone Renderer Window *)
type RenderStandaloneWindow () as window =
    inherit OpenTK.GameWindow(
        600, 600, GraphicsMode.Default, "title",
        GameWindowFlags.Default, DisplayDevice.Default, 3, 3, GraphicsContextFlags.Default)

    let renderer = new DawnLib.Renderers.GL3.Renderer()
    let voxels = new BaseGameSystem<DawnLib.Voxels.BlockChunk>()
    let meshes = voxels.Map DawnLib.Voxels.chunkToMesh
    let vbos = meshes.MapWithMainThreadPoll window (fun f -> new VBO(f))

    member this.Meshes = meshes
    member this.Voxels = voxels

    override this.OnLoad e =
        base.OnLoad e
        this.X <- (DisplayDevice.Default.Bounds.Width - this.Width) / 2;
        this.Y <- (DisplayDevice.Default.Bounds.Height - this.Height) / 2;
        DawnLib.App.QuitKeyBehavior this
        DawnLib.App.ScreenShotBehavior this
        DawnLib.App.TrackBallCameraBehavior this renderer
        DawnLib.App.MeshDebugStats this meshes
        DawnLib.App.ToggleRenderOptions this renderer

    override this.OnRenderFrame e =
        base.OnRenderFrame e
        GL.ClearColor(Color4.CornflowerBlue)
        GL.Clear(ClearBufferMask.ColorBufferBit ||| ClearBufferMask.DepthBufferBit)
        for vbo in vbos.Items do
            renderer.Draw vbo
        this.SwapBuffers()
