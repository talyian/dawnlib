module DawnLib.Loaders.OBJFile
open DawnLib.Mesh
// open System
open System.IO
open OpenTK

type FileInputPattern = F with 
    static member ($) (_, filename: string) = File.OpenRead(filename) :> Stream
    static member ($) (_, str: Stream) = str

let inline loadOBJ varstream =
    let m = new Mesh()
    let stream : Stream = F $ varstream
    let r = new StreamReader (stream)
    let lines = seq { while true do yield r.ReadLine() } |> Seq.takeWhile ((<>) null)
    let v3 (line:string[]) = Vector3(float32 <| line.[1], float32 <| line.[2], float32 <| line.[3])
    let v line = m.vertices.Add(new Vertex(col=Graphics.Color4.White,pos=v3 line))
    let f line =
        let indices = [| for i in Array.tail line -> int i - 1 |]
        if indices.Length = 3 then m.indices.AddRange(indices)
        else m.indices.AddRange([0;1;2;0;2;3] |> List.map (fun x -> indices.[x]))

    for line in lines do
        let line = line.Split(' ')
        match line.[0] with
        | "v" -> v line
        | "f" -> f line
        | "g" -> ()
        | x -> printfn "unknown OBJ command: %O" x
    m.CalcNormals()
    m
