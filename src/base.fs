module DawnLib.Base
#nowarn "9"

open OpenTK
open OpenTK.Graphics
open OpenTK.Graphics.OpenGL
open System.Collections.Generic
open System.Linq

type Mesh = Mesh.Mesh
type Box = Mesh.Box

type SpatialIndex = interface
    abstract member Add : Vector3 -> unit
    abstract member Add : Box -> unit
    abstract member Remove : Box -> unit
    abstract member Find: Box option -> seq<Box>
end

type ListIndex () =
    let items = new ResizeArray<Box>()
    member this.Items = items
    interface SpatialIndex with
        member this.Remove x = ignore (items.Remove x)
        member this.Add x = items.Add x
        member this.Add (p:Vector3) = items.Add(Box(p, p, new Mesh()))
        member this.Find x =
            match x with
            | None -> items :> seq<_>
            | Some(x) -> items |> Seq.filter(Mesh.Box.intersect x)

type OctreeNode (parent:OctreeNode) =
    let nodes = Array.zeroCreate 8
    let items = new ResizeArray<Box>()

let strip_to_list strip =
    let rec foo result = function
    | a::b::c::xs -> foo (a::b::c::result) (b::c::xs)
    | _ -> result
    foo [] strip

type V3 = Vector3
let bary n = let mutable v = Vector3.Zero in v.[n] <- 1.0f; v

let bridged a b = [
    for (a,b),(c,d) in Seq.pairwise <| Seq.zip a b do
        yield! [a;b;c]
        yield! [d;c;b]
]

type Cube () =
    member val col = Color4.Wheat with get, set
    member this.Triangles =
        let verts = [
            for x in [0.f; 1.f] do
            for y in [0.f; 1.f] do
            for z in [0.f; 1.f] do yield V3(x,y,z) ]
        let indices = [
            0; 1; 2; 3
            4; 5; 6; 7
            0; 1; 4; 5;
            2; 3; 6; 7;
            0; 2; 4; 6;
            1; 3; 5; 7 ]
        let rec loop idx = match idx with
                           | a::b::c::d::xs -> a::b::c::b::c::d::loop xs
                           | _ -> []
        [for i in loop indices  -> Mesh.Vertex(col=this.col,pos=verts.[i])]
