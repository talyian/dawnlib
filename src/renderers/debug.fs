namespace DawnLib.Renderers

open DawnLib.Base
open OpenTK
open OpenTK.Graphics.OpenGL;

type DebugRenderer () =
    interface DawnLib.Renderer.IRenderer with
      member val Proj = Matrix4.Identity with get, set
      member val Modelview = Matrix4.Identity with get, set
      member this.Init () = ()
      member this.Draw (mesh:Mesh) =
         GL.Begin(PrimitiveType.Triangles)
         for i in mesh.indices do
            let v = mesh.vertices.[i]
            GL.Color4(v.col)
            GL.Normal3(v.nor)
            GL.TexCoord2(v.uv)
            GL.Vertex3(v.pos)
         GL.End()
