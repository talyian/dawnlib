module DawnLib.Renderer

open DawnLib.Base
open OpenTK
open OpenTK.Graphics.OpenGL;

type IRenderer =
    abstract Proj: Matrix4 with get, set
    abstract Modelview : Matrix4 with get, set
    abstract Init : unit -> unit
    abstract Draw : Mesh -> unit

type DebugWindow (renderer: IRenderer) =
    inherit OpenTK.GameWindow()
    member val Meshes : Mesh list = []
    override this.OnRenderFrame e =
        GL.MatrixMode(MatrixMode.Projection)
        let m = renderer.Proj in GL.LoadMatrix(ref m)
        GL.MatrixMode(MatrixMode.Modelview)
        let m = renderer.Modelview in GL.LoadMatrix(ref m)
        GL.ClearColor(Graphics.Color4.CornflowerBlue)
        GL.Clear(ClearBufferMask.ColorBufferBit ||| ClearBufferMask.DepthBufferBit)
        for m in this.Meshes do renderer.Draw m
        this.SwapBuffers()
