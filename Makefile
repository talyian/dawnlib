LIBS= -r packages/OpenTK/lib/net20/OpenTK.dll

# If sourcefiles is regenerated, we have to re-run make to reload FS_FILES and FS_ALL
bin/dawnlib.dll: bin/sourcefiles src/* src/*/*
	@mkdir -p bin
	make bin/dawnlib.dll.PHONY

.PHONY: bin.dawnlib.dll.PHONY
bin/dawnlib.dll.PHONY: FS_FILES= $(shell cat bin/sourcefiles)
bin/dawnlib.dll.PHONY: FS_ALL=$(shell ls `cat bin/sourcefiles`)
bin/dawnlib.dll.PHONY: ${FS_ALL} bin/OpenTK.dll
	fsharpc ${FS_FILES} --debug+ -a -o bin/dawnlib.dll ${LIBS}

.PHONY: test
test: bin/dawnlib.dll examples/test.fsx
	build/fsi examples/test.fsx

voxel: bin/dawnlib.dll examples/voxel.fsx
	build/fsi examples/voxel.fsx

bin/OpenTK.dll: packages packages/OpenTK/lib/net20/OpenTK.dll
	cp packages/OpenTK/lib/net20/OpenTK.dll bin/OpenTK.dll

Paket:
	nuget install -x Paket

packages: Paket
	mono Paket/tools/paket.exe install

# used to dynamically build dependencies from the fsproj
bin/sourcefiles: dawnlib.fsproj build/xml.fsx
	@mkdir -p bin
	build/fsi build/xml.fsx < dawnlib.fsproj > bin/sourcefiles
