#version 330

layout (location=0) in vec3 pos;
layout (location=1) in vec4 col;
layout (location=2) in vec3 nor;
layout (location=3) in vec2 uv;
layout (location=4) in vec3 bary;

out vec4 _col;
out vec3 _pos;
out vec3 _bary;
out vec3 _nor;

uniform mat4 mv;
uniform mat4 p;
uniform vec3 camera;
uniform vec4 data;

struct SHC{
    vec3 L00, L1m1, L10, L11, L2m2, L2m1, L20, L21, L22;
};

SHC square = SHC(
    vec3(  0.871297f,  0.875222f,  0.864470f ),
    vec3(  0.175058f,  0.245335f,  0.312891f ),
    vec3(  0.034675f,  0.036107f,  0.037362f ),
    vec3( -0.004629f, -0.029448f, -0.048028f ),
    vec3( -0.120535f, -0.121160f, -0.117507f ),
    vec3(  0.003242f,  0.003624f,  0.007511f ),
    vec3( -0.028667f, -0.024926f, -0.020998f ),
    vec3( -0.077539f, -0.086325f, -0.091591f ),
    vec3( -0.161784f, -0.191783f, -0.219152f )
);

//         // Constants for Grace Cathedral lighting
SHC cathedral = SHC(
    vec3(  0.78908f,  0.43710f,  0.54161f ),
    vec3(  0.39499f,  0.34989f,  0.60488f ),
    vec3( -0.33974f, -0.18236f, -0.26940f ),
    vec3( -0.29213f, -0.05562f,  0.00944f ),
    vec3( -0.11141f, -0.05090f, -0.12231f ),
    vec3( -0.26240f, -0.22401f, -0.47479f ),
    vec3( -0.15570f, -0.09471f, -0.14733f ),
    vec3(  0.56014f,  0.21444f,  0.13915f ),
    vec3(  0.21205f, -0.05432f, -0.30374f )
                    );
//         // Constants for Eucalyptus Grove lighting
SHC grove = SHC(
            vec3(  0.3783264f,  0.4260425f,  0.4504587f ),
            vec3(  0.2887813f,  0.3586803f,  0.4147053f ),
            vec3(  0.0379030f,  0.0295216f,  0.0098567f ),
            vec3( -0.1033028f, -0.1031690f, -0.0884924f ),
            vec3( -0.0621750f, -0.0554432f, -0.0396779f ),
            vec3(  0.0077820f, -0.0148312f, -0.0471301f ),
            vec3( -0.0935561f, -0.1254260f, -0.1525629f ),
            vec3( -0.0572703f, -0.0502192f, -0.0363410f ),
            vec3(  0.0203348f, -0.0044201f, -0.0452180f )
                );

//         // Constants for St. Peter's Basilica lighting
SHC basilica = SHC(
            vec3(  0.3623915f,  0.2624130f,  0.2326261f ),
            vec3(  0.1759130f,  0.1436267f,  0.1260569f ),
            vec3( -0.0247311f, -0.0101253f, -0.0010745f ),
            vec3(  0.0346500f,  0.0223184f,  0.0101350f ),
            vec3(  0.0198140f,  0.0144073f,  0.0043987f ),
            vec3( -0.0469596f, -0.0254485f, -0.0117786f ),
            vec3( -0.0898667f, -0.0760911f, -0.0740964f ),
            vec3(  0.0050194f,  0.0038841f,  0.0001374f ),
            vec3( -0.0818750f, -0.0321501f,  0.0033399f )
                   );

//         // Constants for Uffizi Gallery lighting
SHC gallery = SHC(
            vec3(   0.3168843f,  0.3073441f,  0.3495361f ),
            vec3(   0.3711289f,  0.3682168f,  0.4292092f ),
            vec3(  -0.0034406f, -0.0031891f, -0.0039797f ),
            vec3(  -0.0084237f, -0.0087049f, -0.0116718f ),
            vec3(  -0.0190313f, -0.0192164f, -0.0250836f ),
            vec3(  -0.0110002f, -0.0102972f, -0.0119522f ),
            vec3(  -0.2787319f, -0.2752035f, -0.3184335f ),
            vec3(   0.0011448f,  0.0009613f,  0.0008975f ),
            vec3(  -0.2419374f, -0.2410955f, -0.2842899f )
                  );

//         // Constants for Galileo's tomb lighting

//         // Constants for Vine Street kitchen lighting
SHC kitchen = SHC(
            vec3(  0.6396604f,  0.6740969f,  0.7286833f ),
            vec3(  0.2828940f,  0.3159227f,  0.3313502f ),
            vec3(  0.4200835f,  0.5994586f,  0.7748295f ),
            vec3( -0.0474917f, -0.0372616f, -0.0199377f ),
            vec3( -0.0984616f, -0.0765437f, -0.0509038f ),
            vec3(  0.2496256f,  0.3935312f,  0.5333141f ),
            vec3(  0.3813504f,  0.5424832f,  0.7141644f ),
            vec3(  0.0583734f,  0.0066377f, -0.0234326f ),
            vec3( -0.0325933f, -0.0239167f, -0.0330796f )
                  );

//         // Constants for Breezeway lighting
SHC breezeway = SHC(
            vec3(  0.3175995f,  0.3571678f,  0.3784286f ),
            vec3(  0.3655063f,  0.4121290f,  0.4490332f ),
            vec3( -0.0071628f, -0.0123780f, -0.0146215f ),
            vec3( -0.1047419f, -0.1183074f, -0.1260049f ),
            vec3( -0.1304345f, -0.1507366f, -0.1702497f ),
            vec3( -0.0098978f, -0.0155750f, -0.0178279f ),
            vec3( -0.0704158f, -0.0762753f, -0.0865235f ),
            vec3(  0.0242531f,  0.0279176f,  0.0335200f ),
            vec3( -0.2858534f, -0.3235718f, -0.3586478f )
                    );

//         // Constants for Campus Sunset lighting
SHC campus = SHC(
            vec3(  0.7870665f,  0.9379944f,  0.9799986f ),
            vec3(  0.4376419f,  0.5579443f,  0.7024107f ),
            vec3( -0.1020717f, -0.1824865f, -0.2749662f ),
            vec3(  0.4543814f,  0.3750162f,  0.1968642f ),
            vec3(  0.1841687f,  0.1396696f,  0.0491580f ),
            vec3( -0.1417495f, -0.2186370f, -0.3132702f ),
            vec3( -0.3890121f, -0.4033574f, -0.3639718f ),
            vec3(  0.0872238f,  0.0744587f,  0.0353051f ),
            vec3(  0.6662600f,  0.6706794f,  0.5246173f )
                 );

//         // Constants for Funston Beach Sunset lighting

SHC beach = SHC(
    vec3( 0.6841148,  0.6929004,  0.7069543),
    vec3( 0.3173355,  0.3694407,  0.4406839),
    vec3(-0.1747193, -0.1737154, -0.1657420),
    vec3(-0.4496467, -0.4155184, -0.3416573),
    vec3(-0.1690202, -0.1703022, -0.1525870),
    vec3(-0.0837808, -0.0940454, -0.1027518),
    vec3(-0.0319670, -0.0214051, -0.0147691),
    vec3( 0.1641816,  0.1377558,  0.1010403),
    vec3( 0.3697189,  0.3097930,  0.2029923)
);

SHC tomb = SHC(
    vec3( 1.0351604,  0.7603549,  0.7074635),
    vec3( 0.4442150,  0.3430402,  0.3403777),
    vec3(-0.2247797, -0.1828517, -0.1705181),
    vec3( 0.7110400,  0.5423169,  0.5587956),
    vec3( 0.6430452,  0.4971454,  0.5156357),
    vec3(-0.1150112, -0.0936603, -0.0839287),
    vec3(-0.3742487, -0.2755962, -0.2875017),
    vec3(-0.1694954, -0.1343096, -0.1335315),
    vec3( 0.5515260,  0.4222179,  0.4162488)
);


vec3 sh_light(vec3 normal, SHC l){
    float x = normal.x;
    float y = normal.y;
    float z = normal.z;

    const float C1 = 0.429043;
    const float C2 = 0.511664;
    const float C3 = 0.743125;
    const float C4 = 0.886227;
    const float C5 = 0.247708;

    return (
        C1 * l.L22 * (x * x - y * y) +
                C3 * l.L20 * z * z +
                C4 * l.L00 -
                C5 * l.L20 +
                2.0 * C1 * l.L2m2 * x * y +
                2.0 * C1 * l.L21  * x * z +
                2.0 * C1 * l.L2m1 * y * z +
                2.0 * C2 * l.L11  * x +
                2.0 * C2 * l.L1m1 * y +
                2.0 * C2 * l.L10  * z
            );
}

void main() {
    _pos = pos;
    _bary = bary;
    _nor = nor;
    SHC shc =
        data.x == 1 ? square :
        data.x == 2 ? cathedral :
        data.x == 3 ? grove :
        data.x == 4 ? basilica :
        data.x == 5 ? gallery :
        data.x == 6 ? breezeway :
        data.x == 7 ? campus :
        data.x == 8 ? beach :
        data.x == 9 ? tomb : square;

    _col = col * vec4(sh_light(nor, shc), 1);
    gl_Position = p * mv * vec4(pos, 1) ;
}
