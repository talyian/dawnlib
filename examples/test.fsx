#I "../bin/Debug"

#r "dawnlib.dll"
#r "OpenTK.dll"

open DawnLib
open OpenTK

let rec sierpinski n d top =
    match n with
        | 0 ->
             let a = top
             let b = top - d * Vector3.UnitY
             let c = b - d * Vector3.UnitX
             let d = b - d * Vector3.UnitZ
             let col = Graphics.Color4.White
             [ a;b;c; c;b;d; b;d;a; a;c;d ] |> List.map (fun p -> Mesh.Vertex(pos=p, col=col))
        | n ->
            [ yield! sierpinski (n-1) (d * 0.5f) top
              yield! sierpinski (n-1) (d * 0.5f) (top - d * 0.5f * Vector3.UnitY)
              yield! sierpinski (n-1) (d * 0.5f) (top - d * 0.5f * Vector3(1.0f, 1.0f, 0.0f))
              yield! sierpinski (n-1) (d * 0.5f) (top - d * 0.5f * Vector3(0.0f, 1.0f, 1.0f)) ]
let mesh = Base.Mesh()
let verts = sierpinski 3 1.0f (Vector3.One)
mesh.vertices.AddRange(verts)
mesh.indices.AddRange([0 .. verts.Length - 1])
mesh.CalcNormals()

let window = new DawnLib.Apps.RenderApp.RenderStandaloneWindow()
window.Meshes.Add(mesh)
window.Run()
