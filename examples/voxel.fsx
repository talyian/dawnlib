#I "../bin/Debug"
#r "dawnlib.dll"
#r "OpenTK.dll"

open System
open System.IO
open OpenTK

let window = new DawnLib.Apps.RenderApp.RenderStandaloneWindow()
Async.Start(async {
    DawnLib.Loaders.OBJFile.loadOBJ "data/lamp.obj" |> window.Meshes.Add
    DawnLib.Loaders.VOXFile.read "data/level.vox" |> window.Voxels.Add
    DawnLib.Loaders.VOXFile.read "data/monu9.vox"  |> window.Voxels.Add
    DawnLib.Loaders.VOXFile.read "data/castle.vox"  |> window.Voxels.Add
})
window.Run()
